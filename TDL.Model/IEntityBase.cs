using System;

namespace TDL.Model
{
    public interface IEntityBase
    {
        int Id {get; set;}
        DateTime Created {get; set;}
    }
}