using System;

namespace TDL.Model
{
    public class ToDo : IEntityBase
    {
        public int Id {get; set;}
        public string Title {get; set;}
        public string Description {get; set;}
        public DateTime Created {get; set;}
        public bool IsCompleted {get; set;}
    }
}