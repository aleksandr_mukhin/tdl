using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TDL.Data;
using TDL.Model;
using AutoMapper;
using System.Net.Http;

namespace TDL.Api.Controllers
{
    [Route("api/[controller]")]
    public class ToDosController : Controller
    {
        private IToDoRepository _toDoRepository;
        
        public ToDosController(IToDoRepository toDoRepository)
        {
            _toDoRepository = toDoRepository;
        }
    
        [HttpGet]
        public IActionResult Get()
        {
            IEnumerable<ToDo> _todos = _toDoRepository
            .GetAll()
            .Where(s =>!s.IsCompleted)
            .OrderBy(s => s.Created)
            .ToList();
    
            IEnumerable<ToDoViewModel> _todosVM = Mapper.Map<IEnumerable<ToDo>, IEnumerable<ToDoViewModel>>(_todos);
    
            return new OkObjectResult(_todosVM);
        }

        [HttpPost]
        public IActionResult Create([FromBody]ToDoViewModel task)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
    
            ToDo _newToDo = Mapper.Map<ToDoViewModel, ToDo>(task);
            _newToDo.Created = DateTime.Now;
    
            _toDoRepository.Add(_newToDo);
            _toDoRepository.Commit();

            task = Mapper.Map<ToDo, ToDoViewModel>(_newToDo);
    
            CreatedAtRouteResult result = CreatedAtRoute("GetToDo", new { controller = "ToDos", id = task.Id }, task);
            return result;
        }
    
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]ToDoViewModel task)
        {
                if (!ModelState.IsValid)
             {
                 return BadRequest(ModelState);
             }
    
             ToDo _taskDb = _toDoRepository.GetSingle(id);
    
        if (_taskDb == null)
        {
             return NotFound();
        }
        else
        {
                 _taskDb.Title = task.Title;
                 _taskDb.Description = task.Description;
                 _taskDb.IsCompleted = task.IsCompleted;

                 _toDoRepository.Commit();
        }
    
             task = Mapper.Map<ToDo, ToDoViewModel>(_taskDb);
    
             return new NoContentResult();
         }
    
         [HttpDelete("{id}")]
         public IActionResult Delete(int id)
         {
             ToDo _taskDb = _toDoRepository.GetSingle(id);
    
             if (_taskDb == null)
             {
                 return new NotFoundResult();
             }
             else
             {
                 _toDoRepository.Delete(_taskDb);
    
                 _toDoRepository.Commit();
    
                 return new NoContentResult();
             }
         }
    }
}