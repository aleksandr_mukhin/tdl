using System;
using FluentValidation;

public class ToDoViewModelValidator : AbstractValidator<ToDoViewModel>
{
    public ToDoViewModelValidator()
    {
        RuleFor(t => t.Title).NotEmpty().WithMessage("Title cannot be empty");
    }

    private bool DateTimeIsGreater(DateTime start, DateTime end)
    {
        return end > start;
    }
}