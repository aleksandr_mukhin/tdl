using TDL.Model;

namespace TDL.Data
{
public interface IToDoRepository : IEntityBaseRepository<ToDo> { }
}