using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System.ComponentModel.DataAnnotations.Schema;
using TDL.Model;

namespace TDL.Data
{
    public class TDLDbContext : DbContext
    {
        private static bool _created = false;
        public DbSet<ToDo> ToDos { get; set; }
        
        public TDLDbContext(DbContextOptions options) : base(options) 
        {
            if (!_created)
            {
                _created = true;
                Database.EnsureDeleted();
                Database.EnsureCreated();
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionbuilder)
        {
            optionbuilder.UseSqlite(@"Data Source=d:\Sample.db");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            
            #region ToDo Table
            modelBuilder.Entity<ToDo>().ToTable("todos");
            modelBuilder.Entity<ToDo>().HasKey(t => t.Id);
            modelBuilder.Entity<ToDo>().Property(t => t.Created).HasColumnName("Created").IsRequired();
            modelBuilder.Entity<ToDo>().Property(t => t.Title).HasColumnName("Title").HasMaxLength(100).IsRequired();
            modelBuilder.Entity<ToDo>().Property(t => t.Description).HasColumnName("Description").HasMaxLength(255);
            modelBuilder.Entity<ToDo>().Property(t => t.IsCompleted).HasColumnName("IsCompleted");
            #endregion
            
        }
    }
}