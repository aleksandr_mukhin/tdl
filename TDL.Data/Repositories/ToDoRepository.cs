using TDL.Model;

namespace TDL.Data.Repositories
{
    public class ToDoRepository : EntityBaseRepository<ToDo>, IToDoRepository
    {
        public ToDoRepository(TDLDbContext tdlDbContext) : base(tdlDbContext)
        { }
    }
}