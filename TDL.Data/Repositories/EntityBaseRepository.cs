using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using TDL.Model;

namespace TDL.Data.Repositories
{
public class EntityBaseRepository<T> : IEntityBaseRepository<T> where T : class, TDL.Model.IEntityBase, new()
{
    private TDLDbContext _tdlDbContext;

    // Constructor
    public EntityBaseRepository(TDLDbContext tdlContext)
    {
        _tdlDbContext = tdlContext;
    }

    // Implementations
    public virtual IEnumerable<T> GetAll()
    {
        return _tdlDbContext.Set<T>().AsEnumerable();
    }

    public T GetSingle(int id)
    {
        return _tdlDbContext.Set<T>().FirstOrDefault(x => x.Id == id);
    }
    public virtual void Add(T entity)
    {
        EntityEntry dbEntityEntry = _tdlDbContext.Entry<T>(entity);
        _tdlDbContext.Set<T>().Add(entity);
    }
 
    public virtual void Update(T entity)
    {
        EntityEntry dbEntityEntry = _tdlDbContext.Entry<T>(entity);
        dbEntityEntry.State = EntityState.Modified;
    }
    public virtual void Delete(T entity)
    {
        EntityEntry dbEntityEntry = _tdlDbContext.Entry<T>(entity);
        dbEntityEntry.State = EntityState.Deleted;
    }
 
    public virtual void Commit()
    {
        _tdlDbContext.SaveChanges();
    }
}
}